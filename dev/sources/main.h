///////////////////////////////////////////////////////////////////////////////

//	the iqueued daemon transfers newly written files from a source directory
//	named on the command line to the immediate subdirectories of a target
//	directory named on the command line.

///////////////////////////////////////////////////////////////////////////////

#ifndef vBMQhpMEaPrfJCVBzAbtlSG5TPq5mEF
#define vBMQhpMEaPrfJCVBzAbtlSG5TPq5mEF

//	variables
//	=========

//	this is the shut-down flag.  if i find the flag is set to true (non-zero)
//	then i should cleanly and immediately shut down.  since a signal handler
//	may set the flag, i qualify the flag as volatile to exclude it from
//	compiler optimisation and type it as sig_atomic_t to ensure it changes
//	atomically.

volatile sig_atomic_t shut_down_flag ;

//	this is the write end of a pipe to which a signal handler can write each
//	signal it receives.  if i include the read end of the pipe in an epoll then
//	i can discover whether the handler has received a signal in the interval
//	before reading from the epoll (and hence blocking) but after last checking
//	for signals prior to reading.

int signal_pipe_write ;

#endif
