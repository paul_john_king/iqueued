////////////////////////////////////////////////////////////////

//	an event handler is a function that processes an individual file system
//	event.

////////////////////////////////////////////////////////////////

#ifndef SrtuMKHvr4oAuc9G9WsStkvxaDa5MTI
#define SrtuMKHvr4oAuc9G9WsStkvxaDa5MTI

//	if
//
//		pointer «event_buffer_p» indicates a close-write-event buffer,
//
//		«source_path» is a directory path,
//
//		«source_fd» is a file descriptor for «source_path»,
//
//		each member of array «target_path_array» is a directory path,
//
//		each member of array «target_fd_array» is a file descriptor for the
//		corresponding member of «target_path_array»,
//
//		«target_count» is the number of directories in «target_path_array», and
//
//		«source_path» and each member of «target_path_array» are on the same
//		file system
//
//	then the function
//
//		void handle_close_write_event( const char* const  «event_buffer_p» ,
//		                               const char* const  «source_path» ,
//		                               const int          «source_fd» ,
//		                               const int          «target_count» ,
//		                               char* const* const «target_path_array» ,
//		                               const int* const   «target_fd_array» )
//
//	extracts file name «file_name» from the event «event_buffer_p» indicates,
//	hard links «target_path»/«file_name» to «source_path»/«file_name» for each
//	member «target_path» of «target_path_array», and unlinks
//	«source_path»/«file_name».

void handle_close_write_event( const char* const  event_buffer_p ,
                               const char* const  source_path ,
                               const int          source_fd ,
                               const int          target_count ,
                               char* const* const target_path_array ,
                               const int* const   target_fd_array ) ;

#endif
