////////////////////////////////////////////////////////////////

//	a message is a string that i send to the system log with the daemon
//	facility in order to give the user information.  each message has a
//	priority.  the priorities are debug, info, notice, warning or error.

//	a message of priority error informs the user that something seriously wrong
//	has occurred and i will stop as a result.  a message of priority warning
//	informs the user that something seriously wrong has occured but i will not
//	stop as a result.  note, a warning may be as serious as an error, the only
//	difference is that i stop as a result of an error, but do not stop as a
//	result of a warning.

//	a message of priority notice informs the user that something significant
//	has happened.  a message of priority info informs the user that something
//	ordinary has happened.

//	a message of priority debug informs the user about my low-level operation.
//	debug messages are only of interest to the devloper.  

//	verbosity determines whether i actually log a message.  verbosity can be
//	debug, info, notice, warning or priority.  if verbosity is debug then i log
//	messages of every priority.  if verbosity is info then i only log messages
//	of verbosity info, notice, warning or error.  if verbosity is notice then i
//	only log messages of verbosity notice, warning or error.  if verbosity is
//	warning then i only log messages of verbosity warning or error.  if
//	verbosity is error then i only log messages of verbosity error. 

//	by default, verbosity is warning.

////////////////////////////////////////////////////////////////

#ifndef IIqejm9oFDswVuPNUC9hwSq5Wfx23eD
#define IIqejm9oFDswVuPNUC9hwSq5Wfx23eD

//	header files
//	============

//	the global header files.

#include <signal.h>

//	the local header files.

#include "reports.h"

//	defines and macros
//	==================

//	the logging verbosity list is a '|' separated string of the valid
//	verbosities.  this string can be used in usage messages for example.

#define LOGGING_VERBOSITY_LIST ( "debug|info|notice|warning|error" )

//	if INCLUDE_DEBUG is defined then the macro
//
//		log_debug_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//	expands to the function call
//
//		log_function_message( debug , __func__ , «format_string» , «parameter_1» , ... , «parameter_n» )
//
//	(see below) where macro __func__ in turn expands to the name of the
//	function containing the macro.

//	if INCLUDE_DEBUG is not defined then the macro
//
//		log_debug_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//	expands to nothing and there are therefore no debug messages for me to log
//	even if verbosity is debug.  this removes many small function calls with
//	conditional statements from the c source code, and should be done when
//	compiling the source for a production executable.

#ifdef INCLUDE_DEBUG

#define log_debug_message(...) { log_function_message( debug , __func__ , __VA_ARGS__ ) ; }

#else

#define log_debug_message(...)

#endif

//	The macros
//
//		log_info_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_notice_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_warning_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_error_message( «format_string» , «parameter_1» , ... , «parameter_n» )
//
//	expand to the function calls
//
//		log_message( info , «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_message( notice , «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_message( warning , «format_string» , «parameter_1» , ... , «parameter_n» )
//
//		log_message( error , «format_string» , «parameter_1» , ... , «parameter_n» )
//
//	(see below) respectively.

#define log_info_message(...) { log_message( info , __VA_ARGS__ ) ; }

#define log_notice_message(...) { log_message( notice , __VA_ARGS__ ) ; }

#define log_warning_message(...) { log_message( warning , __VA_ARGS__ ) ; }

#define log_error_message(...) { log_message( error , __VA_ARGS__ ) ; }

//	constants and variables
//	=======================

//	the debug, info, notice, warning and error flags determine verbosity.  if
//	debug, info, notice, warning or error are true (i.e., non-zero) then i log
//	messages of priority debug, info, notice, warning or error repectively.  by
//	default, verbosity is warning: i log only messages of priority warning or
//	error.

//	note: these variables may be set by a signal handler, therefore they are
//	qualified volatile to exclude them from compiler optimisation and typed
//	sig_atomic_t to ensure they change atomically.

volatile sig_atomic_t debug ;
volatile sig_atomic_t info ;
volatile sig_atomic_t notice ;
volatile sig_atomic_t warning ;
volatile sig_atomic_t error ;

//	the initialise_logging function
//	===============================

//	the function
//
//		void initialise_logging( char* «identifier» )
//
//	initialises logging with identifier «identifier» prepended to every
//	message.  i must call it before i send messages.

void initialise_logging( char* invocation_name ) ;

//	the finalise_logging function
//	=============================

//	the function
//
//		void finalise_logging( void )
//
//	finalises logging.  i must send messages after i call it.

void finalise_logging( void ) ;

//	the log_function_message function
//	=================================

//	the function
//
//		void log_function_message( const sig_atomic_t «flag» ,
//		                           const char* const  «function_name» , 
//		                           const char* const  «format_string» ,
//		                                              «parameter_1» , 
//		                                              ... ,
//		                                              «parameter_n» )
//
//	logs a message with priority «flag» composed of the function name
//	«function_name» and the substitution of the parameters «parameter_1», ...,
//	«parameter_n» into the format string «format_string» if verbosity permits.
//	for example, if i am called as "iqueued" with process id 4874,
//	INCLUDE_DEBUG is defined, verbosity is debug, integer variable count is 17,
//	and the function "purge" contains the function call
//
//		log_function_message( debug , "purging file '%s' deleted %i entries.\n" , "/bar/baz" , count ) ;
//
//	then execution of the function call logs the message
//
//		iqueued[4874]: purge - purging file '/bar/baz' deleted 17 entries.
//

void log_function_message( const sig_atomic_t flag ,
                           const char* const  function_name ,
                           const char* const  format_string ,
                                              ... ) ;

//	the log_message function
//	========================

//	the function
//
//		void log_message( const sig_atomic_t «flag» ,
//		                  const char* const  «format_string» ,
//		                                     «parameter_1» ,
//		                                     ... ,
//		                                     «parameter_n» )
//
//	logs a message with priority «flag» composed of the substitution of the
//	parameters «parameter_1», ..., «parameter_n» into the format string
//	«format_string» if verbosity permits.  for example, if i am called as
//	"iqueued" with process id 4874, verbosity is info, integer variable count
//	is 17, and the function "purge" contains the function call
//
//		log_message( notice , "purging file '%s' deleted %i entries.\n" , "/bar/baz" , count ) ;
//
//	then execution of the function call logs the message
//
//		iqueued[[4874]: purging file '/bar/baz' deleted 17 entries.
//

void log_message( const sig_atomic_t flag ,
                  const char* const  format_string ,
                                     ... ) ;

//	the set_logging_verbosity function
//	==================================

//	the function
//
//		 report set_logging_verbosity( const char* «priority» )
//
//	sets the verbosity to priority «priority».

report set_logging_verbosity( const char* const priority ) ;

//	the increment_logging_verbosity and decrement_logging_verbosity functions
//	=========================================================================

//	the functions
//
//		void increment_logging_verbosity( const int «signal» )
//
//	and
//
//		void decrement_logging_verbosity( const int «signal» )
//
//	respectively increment or decrement the verbosity.  the signal «signal» is
//	unused, but the function prototypes allow the functions to be used as
//	signal handlers.

void increment_logging_verbosity( const int signal ) ;

void decrement_logging_verbosity( const int signal ) ;

#endif
