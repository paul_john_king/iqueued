//	header files
//	============

//	a feature test macro to expose the vsyslog function in the stdarg.h header
//	file.

#define _BSD_SOURCE

//	the system header files.

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>

//	the local header files.

#include "logging.h"

//	constants and variables
//	=======================

//	the LENGTH constant is the maximum length of a message.

static const int LENGTH = 1024 ;

//	see the header file for descriptions.

volatile sig_atomic_t debug   = 0 ;
volatile sig_atomic_t info    = 0 ;
volatile sig_atomic_t notice  = 0 ;
volatile sig_atomic_t warning = 1 ;
volatile sig_atomic_t error   = 1 ;

//	the initialise_logging function
//	===============================

//	see the header file for a description.

void initialise_logging( char* identifier )
{

	//	i open a file descriptor to the system log that writes messages with
	//	the daemon facility and the identifier prepended.

	openlog( identifier ,
	         LOG_PID ,
	         LOG_DAEMON ) ;

}

//	the finalise_logging function
//	=============================

//	see the header file for a description.

void finalise_logging( void )
{

	//	i close the file descriptr to the system log.

	closelog( ) ;

}

//	the log_function_message function
//	=================================

//	see the header file for a description.

void log_function_message( const sig_atomic_t flag ,
                           const char* const  function_name ,
                           const char* const  format_string ,
                                              ... )
{

	char* format_string_1 ;

	format_string_1 = malloc( strlen( function_name ) +
	                          strlen( format_string ) +
	                          4 ) ;

	strcpy( format_string_1 , function_name ) ;
	strcat( format_string_1 , " - " ) ;
	strcat( format_string_1 , format_string ) ;

	if( flag )
	{

		va_list arguments ;

		va_start( arguments ,
		          format_string ) ;

		vsyslog( LOG_DAEMON | LOG_DEBUG ,
		         format_string_1 ,
		         arguments ) ;

		va_end( arguments ) ;

	}

	free( format_string_1 ) ;

}

//	the log_message function
//	========================

//	see the header file for a description.

void log_message( const sig_atomic_t flag ,
                  const char* const  format_string ,
                                     ... )
{

	if( flag )
	{

		va_list arguments ;

		va_start( arguments ,
		          format_string ) ;

		vsyslog( LOG_DAEMON | LOG_INFO ,
		         format_string ,
		         arguments ) ;

		va_end( arguments ) ;

	}

}

//	the set_logging_verbosity function
//	==================================

//	see the header file for a description.

report set_logging_verbosity( const char* const verbosity )
{

	if( strcmp( verbosity , "debug" ) == 0 )
	{

		//	verbosity is "debug".

		//	i enable the logging of messages of every priority.

		debug   = 1 ;
		info    = 1 ;
		notice  = 1 ;
		warning = 1 ;
		error   = 1 ;

	}
	else if( strcmp( verbosity , "info" ) == 0 )
	{

		//	verbosity is "info".

		//	i enable the logging of messages only of priority info, notice,
		//	warning or error.

		debug   = 0 ;
		info    = 1 ;
		notice  = 1 ;
		warning = 1 ;
		error   = 1 ;

	}
	else if( strcmp( verbosity , "notice" ) == 0 )
	{

		//	verbosity is "notice".

		//	i enable the logging of messages only of priority notice, warning
		//	or error.

		debug   = 0 ;
		info    = 0 ;
		notice  = 1 ;
		warning = 1 ;
		error   = 1 ;

	}
	else if( strcmp( verbosity , "warning" ) == 0 )
	{

		//	verbosity is "warning".

		//	i enable the logging of messages only of priority warning or error.

		debug   = 0 ;
		info    = 0 ;
		notice  = 0 ;
		warning = 1 ;
		error   = 1 ;

	}
	else if( strcmp( verbosity , "error" ) == 0 )
	{

		//	verbosity is "error".

		//	i enable the logging of messages only of priority error.

		debug   = 0 ;
		info    = 0 ;
		notice  = 0 ;
		warning = 0 ;
		error   = 1 ;

	}
	else
	{

		//	verbosity is invalid.

		//	i return an indicative report.

		return create_report( INVALID_LOGGING_VERBOSITY , verbosity ) ;

	}

	return success_report ;

}

//	the increment_logging_verbosity and decrement_logging_verbosity functions
//	=========================================================================

//	see the header file for a description.

void increment_logging_verbosity( const int signal )
{

	//	i ignore the signal.

	(void) signal ;

	//	each priority flag takes the value of the next highest priority flag,
	//	and the highest priority flag takes the value true.

	debug   = info ;
	info    = notice ;
	notice  = warning ;
	warning = error ;
	error   = 1 ;

}

void decrement_logging_verbosity( const int signal )
{

	//	i ignore the signal.

	(void) signal ;

	//	each priority flag takes the value of the next lowest priority flag,
	//	and the lowest priority flag takes the value false.

	error   = warning ;
	warning = notice ;
	notice  = info ;
	info    = debug ;
	debug   = 0 ;

}
