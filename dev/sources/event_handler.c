//	header files
//	============

//	a feature test macro to expose the linkat and unlinkat functions in
//	<unistd.h>.

#define _XOPEN_SOURCE 700

//	the global header files.

#include <sys/inotify.h>
#include <unistd.h>

//	the local header files.

#include "event_handler.h"
#include "logging.h"

//	the handle_close_write_event function
//	=====================================

//	see the header file for a description.

void handle_close_write_event( const char* const  event_buffer_p ,
                               const char* const  source_path ,
                               const int          source_fd ,
                               const int          target_count ,
                               char* const* const target_path_array ,
                               const int* const   target_fd_array )
{

	log_debug_message( "starting." ) ;

	log_debug_message( "event_buffer_p is «%p»." , event_buffer_p ) ;
	log_debug_message( "source_path is «%s»."    , source_path ) ;
	log_debug_message( "source_fd is «%i»."      , source_fd ) ;
	log_debug_message( "target_count is «%i»."   , target_count ) ;

	//	constants and variables
	//	-----------------------

	//	this is the name of the file in the event buffer.

	char* name ;

	//	this is an arbirary return status.

	int ret ;

	//	this is an arbitary index.

	int i ;

	//	extract name
	//	------------

	//	i extract the name of the file from the event buffer.

	name = ( (struct inotify_event*) event_buffer_p )->name ;

	log_debug_message( "name is «%s»." , name ) ;

	log_info_message( "source file: «%s/%s»." , source_path , name ) ;

	//	link target files
	//	-----------------

	for( i = 0 ;
	     i < target_count ;
	     ++i )
	{

		log_debug_message( "target_path_array[ %i ] is «%s»." , i , target_path_array[ i ] ) ;
		log_debug_message( "target_fd_array[ %i ] is «%i»."   , i , target_fd_array[ i ] ) ;

		//	i try to hard link the target file to the source file.

		ret = linkat( source_fd ,
		              name ,
		              target_fd_array[ i ] ,
		              name ,
		              0 ) ;

		log_debug_message( "linkat returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and restart the loop.

			log_warning_message( create_report( LINKAT_FAILURE ,
			                                    name ,
			                                    target_fd_array[ i ] ,
			                                    name ,
			                                    source_fd ).string ) ;

			continue ;

		}

		log_info_message( "target file: «%s/%s»." , target_path_array[ i ] , name ) ;

	}

	//	unlink source file
	//	------------------

	//	i unlink the source file.

	ret = unlinkat( source_fd ,
	                name ,
	                0 ) ;

	log_debug_message( "unlinkat returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i log a warning message but continue.

		log_warning_message( create_report( UNLINKAT_FAILURE ,
		                                    name ,
		                                    source_fd ).string ) ;

	}

	//	return
	//	------

	return ;

}
