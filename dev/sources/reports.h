///////////////////////////////////////////////////////////////////////////////

//	a report comprises a status and a string.  each function i define that
//	reports back to its caller does so by returning a report, where the report
//	status is a natural number that indicates how the function call fared, and
//	the report string is a (hopefully) humanly-understandable description of
//	the status.  a report is therefore in essence a return status accompanied
//	by a description of the status. 

///////////////////////////////////////////////////////////////////////////////

#ifndef F2bBEfk0jE_a2t5rfOtgkRGqqNxMJY1
#define F2bBEfk0jE_a2t5rfOtgkRGqqNxMJY1

//	defines and macros
//	==================

//	REPORT_STRING_MAX is the maximum number of characters that a report string
//	can contain, including the terminating NULL.

#define REPORT_STRING_MAX ( 1024 )

//	structures, enumerations and type definitions
//	=============================================

//	the report structure and type definition defines the structure of a report
//	and makes it available as a type.

typedef struct
{

	int  status ;

	char string[ REPORT_STRING_MAX ] ;

} report ;

//	the report status enumeration and type definition enumerates and counts all
//	of the report statuses.

typedef const enum
{

	//	success
	//	.......

	//	this is a special report status that indicates that a function call
	//	succeeded, and i explicitly give it the value 0.

	//	NOTE: this must always be the first element of the enumeration.

	SUCCESS = 0 ,

	//	command line failures
	//	.....................

	//	the command line has an invalid option.  parameters:
	//
	//		the option character.
	//

	INVALID_COMMAND_LINE_OPTION ,

	//	the command line has a valid option without an obligatory value.  parameters:
	//
	//		the option character.
	//

	MISSING_COMMAND_LINE_VALUE ,

	//	the command line option has too few arguments.  parameters:
	//
	//		the number of arguments.
	//

	INSUFFICIENT_COMMAND_LINE_ARGUMENTS ,

	//	the logging verbosity is invalid.  parameters:
	//
	//		the value of the logging verbosity command-line option.
	//

	INVALID_LOGGING_VERBOSITY ,

	//	the event buffer factor is invalid.  parameters:
	//
	//		the value of the event buffer factor command-line option.
	//

	INVALID_EVENT_BUFFER_FACTOR ,

	//	limit failures
	//	...............

	//	i could not find the maximum number of bytes (excluding the terminating
	//	null byte) permitted in a filename in a given directory.  parameters:
	//
	//		a path that names the directory.
	//

	NAME_MAX_FAILURE ,

	//	file descriptor & pipe failures
	//	...............................

	//	i could not open a file descriptor to a path. parameters:
	//
	//		the path.
	//

	OPEN_FD_FAILURE ,

	//	i could not close a file descriptor. parameters:
	//
	//		the file descriptor.
	//

	CLOSE_FD_FAILURE ,

	//	i could not open a pipe.  paramters: none.

	OPEN_PIPE_FAILURE ,

	//	directory stream failures
	//	.........................

	//	i could not open a directory stream to a path. parameters:
	//
	//		the path.
	//

	OPEN_STREAM_FAILURE ,

	//	i could not open a directory stream to a file descriptor.  parameters:
	//
	//		the file descriptor.
	//

	FDOPEN_STREAM_FAILURE ,

	//	i could not read from a directory stream. parameters:
	//
	//		the address of the stream.
	//

	READ_STREAM_FAILURE ,

	//	i could not close a directory stream. parameters:
	//
	//		the address of the stream.
	//

	CLOSE_STREAM_FAILURE ,

	//	working directory failures
	//	..........................

	//	i could not change my working directory to a path.  parameters:
	//
	//		the path.
	//

	CHDIR_FAILURE ,

	//	i could not change my working directory to a file descriptor.
	//	parameters:
	//
	//		the file descriptor.
	//

	FCHDIR_FAILURE ,

	//	stat failures
	//	.............

	//	i could not stat a path.  parameters:
	//
	//		the path.
	//

	STAT_FAILURE ,

	//	i could not stat the concatenation of two paths.  parameters:
	//
	//		the first path (without a trailing "/").
	//
	//		the second path (without a leading "/").
	//

	STAT2_FAILURE ,

	//	two paths are not on the same device.  parameters:
	//
	//		the first path.
	//
	//		the second path.
	//

	DEVICE_FAILURE ,

	//	permission failures
	//	...................

	//	i do not have read, write and access permissions to a file named by a path.  parameters:
	//
	//		the path
	//

	RWX_PERMISSION_FAILURE ,

	//	link failures
	//	.............

	//	i could not hard link a target path relative to a target file
	//	descriptor to a source path relative to a source file descriptor.
	//	parameters:
	//
	//		the target path.
	//
	//		the target file descriptor.
	//
	//		the source path.
	//
	//		the source file descriptor.

	LINKAT_FAILURE ,

	//	i could not unlink a path relative to a file descriptor.  parameters:
	//
	//		the path.
	//
	//		the file descriptor.

	UNLINKAT_FAILURE ,

	//	path failures
	//	.............

	//	i could not convert a path to a canonicalised absolute path.  paremeters:
	//
	//		the path.
	//

	REALPATH_FAILURE ,

	//	memory failures
	//	...............

	//	i could not allocate memory.  parameters: none.

	ALLOCATE_MEMORY_FAILURE ,

	//	process failures
	//	................

	//	i could not fork a new process.  parameters: none.

	FORK_FAILURE ,

	//	i could not create a new session.  parameters: none.

	SESSION_FAILURE ,

	//	signal failures
	//	...............

	//	i could not initialise a signal set. parameters:
	//
	//		the address of the signal set.
	//

	INIT_SIGNAL_SET_FAILURE ,

	//	i could not set the signal mask to a signal set.  parameters:
	//
	//		the adddress of the signal set.

	SET_SIGNAL_MASK_FAILURE ,

	//	i could not set the action of a signal to a function.  parameters:
	//
	//		the signal.
	//
	//		the address of the function.
	//

	SET_SIGNAL_ACTION_FAILURE ,

	//	epoll failures
	//	..............

	//	i could not create an epoll instance.  parameters: none.

	CREATE_EPOLL_FAILURE ,

	//	i could not add a file descriptor to an epoll.  parameters:
	//
	//		the epoll.
	//
	//		the file descriptor.
	//

	ADD_EPOLL_FD_FAILURE ,

	//	an epoll could not wait for events.  parameters:
	//
	//		the epoll.
	//

	EPOLL_WAIT_FAILURE ,

	//	an epoll timed out before receiving an event.  parameters:
	//
	//		the epoll.
	//

	EPOLL_TIME_OUT_FAILURE ,

	//	an epoll received an unknown event.  parameters:
	//
	//		the epoll.
	//
	//		the event file descriptor.
	//

	UNKNOWN_EPOLL_EVENT_FAILURE ,

	//	i could not close an epoll. parameters:
	//
	//		the epoll.
	//

	CLOSE_EPOLL_FAILURE ,

	//	inotify failures
	//	................

	//	i could not open an event queue.  parameters: none.

	OPEN_EVENT_QUEUE_FAILURE ,

	//	i could not close an event queue.  parameters:
	//
	//		the event queue.
	//

	CLOSE_EVENT_QUEUE_FAILURE ,

	//	i could not add an event watch watching a file to an event queue.
	//	parameters:
	//
	//		a path that names the file.
	//
	//		the event queue.
	//

	ADD_EVENT_WATCH_FAILURE ,

	//	i could not remove an event watch from an event queue.  parameters:
	//
	//		the event watch.
	//
	//		the event queue.
	//

	REMOVE_EVENT_WATCH_FAILURE ,

	//	i could not read from an event queue. parameters:
	//
	//		the event queue.
	//

	READ_EVENT_FAILURE ,

	//	i read a null event from an event queue.  parameters:
	//
	//		the event queue.
	//

	READ_NULL_EVENT_FAILURE ,

	//	an event watch of an event queue ended.  parameters:
	//
	//		the event queue.
	//
	//		the event watch.
	//

	WATCH_ENDED_FAILURE ,

	//	return status count
	//	...................

	//	RETURN_STATUS_COUNT is not a report status, but a count of the report
	//	statuses in the enumeration. 

	//	NOTE: this must always be the last element of the enumeration.

	RETURN_STATUS_COUNT
	
} const report_status ;

//	constants and variables
//	=======================

//	this pre-created report has the status SUCCESS.  a function call that
//	succeeds can return this instead of creating a new report.

report success_report ;

//	the initialise_reports function
//	===============================

//	the function
//
//		void initialise_reports( void )
//
//	initialises the reports.  it must be called before reports are created.

void initialise_reports( void ) ;

//	the create_report function
//	==========================

//	the function
//
//		report create_report( const int «status» ,
//		                                «parameter_1» ,
//		                                ... ,
//		                                «parameter_n» )
//
//	creates a report from the report status «status» and the parameters
//	«parameter_1», ..., «parameter_n».  the description of each report status
//	lists the parameters it requires.

report create_report( const int status , ... ) ;

#endif
