//	header files
//	============

//	the system header files.

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

//	the local headers files.

#include "reports.h"

//	constants and variables
//	=======================

//	the report format string array is an array of report format strings.  if
//	integer «status» is a report status then the value in the array at index
//	«status» is a format string, possibly containing conversion specifications,
//	that describes the report status, otherwise the value is the empty string.

static char report_format_string_array[ RETURN_STATUS_COUNT ][ REPORT_STRING_MAX ] ;

//	the add_format_string function
//	==============================

//	the function
//
//		void add_format_string( const report_status «status» ,
//		                        const char* const   «string» )
//
//	places the report format string «string» at index «status» in the report
//	format string array.

static void add_format_string( const report_status status ,
                               const char* const   string )
{

	strncpy( report_format_string_array[ status ] ,
	         string ,
	         REPORT_STRING_MAX) ;

}

//	the initialise_reports function
//	===============================

//	see the header file for a description.

void initialise_reports( void )
{

	//	constants and variables
	//	-----------------------

	//	this is an arbitrary index.

	int i ;

	//	null entries
	//	------------

	//	for safety's sake, i initially place the empty string at each index of
	//	the report format string array.

	for( i = 0 ;
	     i < RETURN_STATUS_COUNT ;
	     ++i )
	{

		report_format_string_array[ i ][ 0 ] = '\0' ;

	}

	//	non-null entries
	//	----------------

	//	i then place an appropriate report format string at each index of the
	//	report format string array.

	//	success
	//	.......

	add_format_string( SUCCESS ,
	                   "success." ) ;

	//	command line failures
	//	.....................

	add_format_string( INVALID_COMMAND_LINE_OPTION ,
	                   "the command line option «-%1$c» is invalid." ) ;

	add_format_string( MISSING_COMMAND_LINE_VALUE ,
	                   "the valid command line option «-%1$c» is missing an obligatory value." ) ;

	add_format_string( INSUFFICIENT_COMMAND_LINE_ARGUMENTS ,
	                   "the command line should contain 2 or more arguments, but contains %1$i." ) ;

	add_format_string( INVALID_LOGGING_VERBOSITY ,
	                   "«%1$s» is not a valid logging verbosity." ) ;

	add_format_string( INVALID_EVENT_BUFFER_FACTOR ,
	                   "«%1$s» is not a valid event buffer factor." ) ;

	//	limit failures
	//	..............

	add_format_string( NAME_MAX_FAILURE ,
	                   "i could not find the maximum number of bytes (excluding the terminating null byte) permitted in a filename in the directory «%1$s»." ) ;

	//	file descriptor & pipe failures
	//	...............................

	add_format_string( OPEN_FD_FAILURE ,
	                   "i could not open a file descriptor to the path «%1$s».  %m." ) ;

	add_format_string( CLOSE_FD_FAILURE ,
	                   "i could not close the file descriptor «%1$i».  %m." ) ;

	add_format_string( OPEN_PIPE_FAILURE ,
	                   "i could not open a pipe.  %m." ) ;

	//	directory stream failures
	//	.........................

	add_format_string( OPEN_STREAM_FAILURE ,
	                   "i could not open a directory stream to the path «%1$s».  %m." ) ;

	add_format_string( FDOPEN_STREAM_FAILURE ,
	                   "i could not open a directory stream to the file descriptor «%1$i».  %m." ) ;

	add_format_string( READ_STREAM_FAILURE ,
	                   "i could not read from the directory stream at «%1$p».  %m." ) ;

	add_format_string( CLOSE_STREAM_FAILURE ,
	                   "i could not close the directory stream at «%1$p».  %m." ) ;

	//	working directory failures
	//	..........................

	add_format_string( CHDIR_FAILURE ,
	                   "i could not change my working directory to the path «%1$s».  %m." ) ;

	add_format_string( FCHDIR_FAILURE ,
	                   "i could not change my working directory to the file descriptor «%1$i».  %m." ) ;

	//	stat failures
	//	.............

	add_format_string( STAT_FAILURE ,
	                   "i could not stat the path «%1$s».  %m." ) ;

	add_format_string( STAT2_FAILURE ,
	                   "i could not stat the path «%1$s/%2$s».  %m." ) ;

	add_format_string( DEVICE_FAILURE ,
	                   "the paths «%1$s» and «%2$s» are not on the same device." ) ;

	//	permission failure
	//	..................

	add_format_string( RWX_PERMISSION_FAILURE ,
	                   "i do not have read, write and access permissions to the file named by «%1$s»." ) ;

	//	link failures
	//	.............

	add_format_string( LINKAT_FAILURE ,
	                   "i could not hard link the path «%1$s» relative to the file descriptor «%2$i» to the path «%3$s» relative to the file descriptor «%4$i».  %m" ) ;

	add_format_string( UNLINKAT_FAILURE ,
	                   "i could not unlink the path «%1$s» relative to the file descriptor «%2$i».  %m" ) ;

	//	path failures
	//	.............

	add_format_string( REALPATH_FAILURE ,
	                   "i could not convert the path «%1$s» to a canonicalised absolute path.  %m." ) ;

	//	memory failures
	//	...............

	add_format_string( ALLOCATE_MEMORY_FAILURE ,
	                   "i could not allocate memory.  %m." ) ;

	//	process failures
	//	................

	add_format_string( FORK_FAILURE ,
	                   "i could not fork a new process.  %m." ) ;

	add_format_string( SESSION_FAILURE ,
	                   "i could not create a new session.  %m." ) ;

	//	signal failures
	//	...............

	add_format_string( INIT_SIGNAL_SET_FAILURE ,
	                   "i could not initialise the signal set at «%1$p».  %m." ) ;

	add_format_string( SET_SIGNAL_MASK_FAILURE ,
	                   "i could not set the signal mask to the signal set at «%1$p».  %m." ) ;

	add_format_string( SET_SIGNAL_ACTION_FAILURE ,
	                   "i could not set the action of the signal «%1$i» to the function at «%2$p».  %m." ) ;

	//	epoll failures
	//	..............

	add_format_string( CREATE_EPOLL_FAILURE ,
	                   "i could not create an epoll instance.  %m." ) ;

	add_format_string( ADD_EPOLL_FD_FAILURE ,
	                   "i could not add the file descriptor «%2$i» to the epoll «%1$i».  %m." ) ;

	add_format_string( EPOLL_WAIT_FAILURE ,
	                   "the epoll «%1$i» could not wait for events.  %m." ) ;

	add_format_string( EPOLL_TIME_OUT_FAILURE ,
	                   "the epoll «%1$i» timed out before receiving an event.  %m." ) ;

	add_format_string( UNKNOWN_EPOLL_EVENT_FAILURE ,
	                   "the epoll «%1$i» received the unknown event «%1$i»." ) ;

	add_format_string( CLOSE_EPOLL_FAILURE ,
	                   "i could not close the epoll «%1$i».  %m." ) ;

	//	inotify failures
	//	................

	add_format_string( OPEN_EVENT_QUEUE_FAILURE ,
	                   "i could not open an event queue.  %m." ) ;

	add_format_string( CLOSE_EVENT_QUEUE_FAILURE ,
	                   "i could not close the event queue «%1$i».  %m." ) ;

	add_format_string( ADD_EVENT_WATCH_FAILURE ,
	                   "i could not add an event watch watching the file «%1$s» to the event queue «%2$i».  %m." ) ;

	add_format_string( REMOVE_EVENT_WATCH_FAILURE ,
	                   "i could not remove the event watch «%1$i» from the event queue «%2$i».  %m." ) ;

	add_format_string( READ_EVENT_FAILURE ,
	                   "i could not read from the event queue «%1$i».  %m." ) ;

	add_format_string( READ_NULL_EVENT_FAILURE ,
	                   "i read a null event from the event queue «%1$i»." ) ;

	add_format_string( WATCH_ENDED_FAILURE ,
	                   "the event watch «%2$i» of the event queue «%1$i» ended." ) ;

	//	success report
	//	--------------

	//	i create the success report.

	success_report = create_report( SUCCESS ) ;

}

//	the create_report function
//	==========================

//	description: see the header file.

report create_report( const int status ,
                                ... )
{

	//	constants and variables
	//	-----------------------

	//	this is the report i create.

	report report ;

	//	this is the bundle of parameters i receive in my variable argument list.

	va_list parameters ;

	//	this is an arbitrary return status.

	int ret ;

	//	report status
	//	-------------

	//	i set the report status.

	report.status = status ;

	//	report string
	//	-------------

	if( SUCCESS <= status &&
	    status  <  RETURN_STATUS_COUNT )
	{

		//	the report status is within the valid range.

		//	i try to create the report string from the appropriate report
		//	string format and the parameters.   the report string can contain
		//	up to REPORT_STRING_MAX characters, including the terminating NULL.
		//	if necessary, the report string will be truncated.

		va_start( parameters ,
		          status ) ;

		ret = vsnprintf( report.string ,
		                 REPORT_STRING_MAX ,
		                 report_format_string_array[ status ] ,
		                 parameters ) ; 

		va_end( parameters ) ;

		if( ret < 0 )
		{

			//	a failure occurred.

			//	i set the report string to "INVALID REPORT STRING.".

			strcpy( report.string ,
			        "INVALID REPORT STRING." ) ;

		}

	}
	else
	{

		//	the report status is not within the valid range.

		//	i set the report string to "INVALID REPORT STATUS."

		strcpy( report.string ,
		        "INVALID REPORT STATUS." ) ;

	}

	//	return
	//	------

	//	i return the report.

	return report ;	

}
