////////////////////////////////////////////////////////////////

//	TODO: check more carefully for errors reading the event queue and signal
//	pipe.

////////////////////////////////////////////////////////////////

//	TODO: write a man page.

////////////////////////////////////////////////////////////////

//	TODO: devise more meaningful reports.

////////////////////////////////////////////////////////////////

//	TODO: if it doesn't already do it, change logging so that if logging is set
//	to debug in production mode, then logging is set to info.  at present, i
//	think it just gets left at the default, which would seem a little
//	counterintuitive to a user.

////////////////////////////////////////////////////////////////

//	TODO: change logging so that warning and error messages have a "warning"
//	and "error" prefix.

////////////////////////////////////////////////////////////////

//	TODO: change logging so that if the standard output and/or standard error
//	are still open then i also write (non-debug?) messages to them as well as
//	standard error.

////////////////////////////////////////////////////////////////

//	TODO: i can perhaps save myself a lot of typing, avoid errors and make the
//	code clearer if i write a private function or a macro that creates a
//	report, logs a message, sets a return status and goes to shutdown.

////////////////////////////////////////////////////////////////

//	TODO: come up with a sweeper that touches each existing file in the source
//	directory after the watch on the event queue is established, and if the
//	event queue overflows.

////////////////////////////////////////////////////////////////

//	TODO: monitor if the source and target paths are deleted or moved onto a
//	different file system, and do something appropriate and robust if they are.
//	such a monitor could perhaps also be used to update paths, provided i can
//	find a way to program a "do this when i am not busy" mechanism.  maybe a
//	time out in the poll loop, so that if the time out expires i know i was not
//	busy, so check a "directory changed" flag, and if set, check the
//	directories.

////////////////////////////////////////////////////////////////

//	TODO: add code to change owner and permissions of the file.

////////////////////////////////////////////////////////////////

//	header files
//	============

//	a feature test macro that exposes the SUSv4 XSI extensions.

#define _XOPEN_SOURCE 700

//	the global header files.

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <unistd.h>

//	the local header files.

#include "event_handler.h"
#include "logging.h"
#include "main.h"

//	defines and macros
//	==================

//	this is the event queue epoll event file descriptor.  i use this to
//	identify the event queue epoll event.

#define EVENT_QUEUE_FD ( 0 )

//	this is the signal pipe epoll event file descriptor.  i use this to
//	identify the signal pipe epoll event.

#define SIGNAL_PIPE_FD ( 1 )

//	global variables
//	================

//	see the header file for a description.  i initially set the flag to false.

volatile sig_atomic_t shut_down_flag = 0 ;

//	see the header file for a description.  i initially set the write end of
//	the pipe to an error value to indicate that i have not yet opened it.  i
//	can subsequently test this value to determine whether or not i should close
//	this end of the pipe.

int signal_pipe_write = -1 ;

//	the set_shut_down_flag function
//	===============================

//	the function

//		void set_shut_down_flag( const int «signal» )

//	sets the shut-down flag to the signal «signal», and writes «signal» to the
//	signal pipe if the write end of the pipe has been opened.  since all
//	signals are non-zero natural numbers, if i register this function to handle
//	signal «signal» then receipt of «signal» sets the shut-down flag to true
//	(and to a value that shows which signal last set it).

static void set_shut_down_flag( const int signal )
{

	shut_down_flag = signal ;

	if( signal_pipe_write != -1 )
	{

		write( signal_pipe_write ,
		       &signal ,
		       sizeof( int ) ) ;

	}

}

//	the main function
//	=================

//	the function

//		int main( int    «argc» ,
//		          char** «argv» )

//	transfers files from a source directory named by the first command line
//	argument in «argv» to target directories named by the subsequent
//	command-line arguments in «argv».

int main( int    argc ,
          char** argv )
{

	//	constants
	//	---------

	//	this is the command-line-options string.   it defines the valid
	//	command-line options.  the optarg function uses this string to parse
	//	and validate the command-line options.  note: the leading '+' character
	//	prevents gnu getopt permuting the command-line arguments, and the
	//	subsequent ':' character suppresses getopt error messages.

	const char* const COMMAND_LINE_OPTIONS = "+:n:v:" ;

	//	variables
	//	---------

	//	this is my return status.  i initially set it to the success status,
	//	modify it as necessary during start up and the poll loop, and return it
	//	after shut down.

	int ret_status = SUCCESS ;

	//	this is my invocation name.  it is the name by which i am invoked with
	//	any leading directory components removed.

	char* invocation_name ;

	//	this is the message buffer factor.  it helps determine the size of the
	//	message buffer (see below).  if the command line contains the "-n"
	//	option then this is set to the integer value of the option, otherwise
	//	it is set to 1.

	long message_buffer_factor = 1 ;

	//	this is the empty signal set.  it contains no posix signals.

	sigset_t empty_signal_set ;

	//	this is the full signal set.  it contains all posix signals.

	sigset_t full_signal_set ;

	//	this is the set shut-down flag action.  i use it to register the set
	//	shut-down flag function to handle a signal.

	struct sigaction set_shut_down_flag_action ;

	//	this is the increment logging verbosity action.  i use it to register
	//	the increment logging verbosity function to handle a signal.

	struct sigaction increment_logging_verbosity_action ;

	//	this is the decrement logging verbosity action.  i use it to register
	//	the decrement logging verbosity function to handle a signal.

	struct sigaction decrement_logging_verbosity_action ;

	//	this is the signal pipe.  a signal handler can write each signal it
	//	receives to this pipe, so that if i include the read end of the pipe in
	//	an epoll then i can discover whether the handler has received a signal
	//	in the interval before reading from the epoll (and hence blocking) but
	//	after last checking for signals prior to reading.  i assign the write
	//	end of the pipe to a global variable so that any signal handler can
	//	access it.

	int signal_pipe[ 2 ] ;

	//	this is the read end of the signal pipe.  i include this in the epoll i
	//	read during the poll loop so that i can discover whether the handler
	//	has received a signal in the interval before reading from the epoll
	//	(and hence blocking) but after last checking for signals prior to
	//	reading.  i initially set the read end of the pipe to an error value to
	//	indicate that i have not yet opened it.  i can subsequently test this
	//	value to determine whether or not i should close this end of the pipe.

	int signal_pipe_read = -1 ;

	//	this is the source path.  it is a canonical absolute path that names
	//	the source directory, the directory i transfer files from.  i initially
	//	set the path to an error value to indicate that i have not yet
	//	allocated heap memory to it.  i can subsequently test this value to
	//	determine whether or not i should release heap memory from the path.

	char* source_path = NULL ;

	//	this is the source file descriptor.  it indicates the source directory.
	//	i initially set the source file descriptor to an error value to
	//	indicate that i have not yet opened it.  i can subsequently test this
	//	value to determine whether or not i should close the file descriptor.

	int source_fd = -1 ;

	//	this is the stat of the source file descriptor.

	struct stat source_stat ;

	//	this is the source device id.  it identifies the device that contains
	//	the source directory.

	dev_t source_device_id ;

	//	this is the target count.  it is the number of directories i transfer
	//	files to.

	int target_count ;

	//	this is the target-path array.  each target path is a canonical
	//	absolute path that names a target directory, a directory i transfer
	//	files to.  i initially set the array to an error value to indicate that
	//	i have not yet allocated heap memory to it.  i can subsequently test
	//	this value to determine whether or not i should release heap memory
	//	from the array (after first releasing any heap memory i have allocated
	//	to its members).

	char** target_path_array = NULL ;

	//	this is the target-file-descriptor array.  each target file descriptor
	//	indicates a target directory.  i initially set the array to an error
	//	value to indicate that i have not yet allocated heap memory to it.  i
	//	can subsequently test this value to determine whether or not i should
	//	release heap memory from the array (after first closing any of its
	//	members i have opened).

	int* target_fd_array = NULL ;

	//	this is the stat of a target file descriptor.

	struct stat target_stat ;

	//	this is the target device id.  it identifies the device that contains a
	//	target directory.

	dev_t target_device_id ;

	//	this is the event queue.  it is a file descriptor that indicates an
	//	inotify instance.  i initially set the event queue to an error value to
	//	indicate that i have not yet opened it.  i can subsequently test this
	//	value to determine whether or not i should close the queue.

	int event_queue = -1 ;

	//	this is the event watch.  it is an inotify watch that writes an event
	//	message to the event queue whenever a file in the source directory
	//	opened for writing is closed.  i initially set the event watch to an
	//	error value to indicate that i have not yet added it to the event
	//	queue.  i can subsequently test this value to determine whether or not
	//	i should remove the watch from the queue.

	int event_watch = -1 ;

	//	this is the epoll.  this epoll monitors the event queue and the signal
	//	pipe during my poll loop.  i set the epoll to an error value to
	//	indicate that it has not been created.

	int epoll = -1 ;

	//	this is the event queue epoll event.  i associate this with the event
	//	queue in the epoll.

	struct epoll_event event_queue_ee ;

	//	this is the signal pipe epoll event.  i associate this with the signal
	//	pipe in the epoll.

	struct epoll_event signal_pipe_ee ;

	//	this is the maximum file name size.  it is the maximum number of bytes
	//	(excluding the terminating null byte) permitted in a filename in a
	//	source directory.

	long max_file_name_size ;

	//	this is the minimum event size. it is the number of bytes in an inotify
	//	event without the optional null-terminated file name.

	size_t min_event_size ;

	//	this is the maximum event size.  it is the number of bytes in an
	//	inotify event with the longest possible optional null-terminated file
	//	name in a source or target directory.

	size_t max_event_size ;

	//	this is the message buffer size.  it is the number of bytes in the
	//	message buffer (see below) and is set to the product of the message
	//	buffer factor and the maximum event size.

	size_t message_buffer_size ;

	//	this is the message buffer.  i read bytes from the event queue and
	//	signal pipe into this buffer.  i set the buffer to NULL to indicate
	//	that it has not been created.  i can subsequently test this value to
	//	determine whether or not i should release heap memory from the buffer.

	char* message_buffer = NULL ;

	//	this is the parent flag.  the flag is set to true if, and only if, i am
	//	the parent process.

	pid_t parent_flag = 1 ;

	//	this is an arbitrary integer.

	int i ;

	//	this is an arbitrary return value.

	int ret ;

	//	this is an arbitrary string.

	char* str ;

	//	this is an abitrary report.

	report rep ;

	//	this is an arbitrary process id.

	pid_t pid ;

	//	this is an arbitrary epoll event.

	struct epoll_event ee ;

	//	this is an arbitrary count of the bytes read into a buffer.

	ssize_t count ;

	//	this is an orbitrary offset into a buffer. 

	ssize_t offset ;

	//	this is an arbitrary pointer.

	void* ptr ;

	//	this is an arbitary file descriptor.

	int fd ;

	//	initialise
	//	==========

	//	i initialise myself and examine my invocation in preparation for
	//	starting up.  i discover my invocation name, initialise reports and
	//	logging, and discover and check the command line options and arguments. 

	//	invocation name
	//	---------------

	//	i discover my invocation name.  i initially set the invocation name to
	//	the first element of argv.  i then read each character of the first
	//	element of argv in order, and whenever i encounter the '/' character, i
	//	set the invocation name to the string starting just after the '/'
	//	character.

	//	note: since the invocation name is a pointer to a character within the
	//	argv array of strings, i need neither allocate heap memory to nor
	//	release heap memory from the invocation name.

	invocation_name = argv[ 0 ] ;

	for( i = 0 ;
	     argv[ 0 ][ i ] != '\0' ;
	     ++i )
	{

		if( argv[ 0 ][ i ] == '/' )
		{

			invocation_name = argv[ 0 ] + i + 1 ;

		}

	}

	//	reports and logging
	//	-------------------

	//	i initialise the reports.

	initialise_reports( ) ;

	//	i initialise the logging.

	initialise_logging( invocation_name ) ;

	//	command-line options
	//	--------------------

	//	i discover and check the command-line options.

	while( 1 )
	{

		//	get option
		//	..........

		//	i try to get the next command-line option.

		ret = getopt( argc ,
		              argv ,
		              COMMAND_LINE_OPTIONS ) ;

		if( ret == -1 )
		{

			//	i found no more command-line options.

			//	i break the while loop.

			break ;

		}

		switch( ret )
		{

			case 'n' :

				//	message buffer factor option
				//	............................

				//	i found the "-n" command-line option.

				//	i try to set the message buffer factor to the value of the
				//	option.

				errno = 0 ;

				message_buffer_factor = strtol( optarg ,
				                                &str ,
				                                10 ) ;

				if( errno != 0 ||
				    *str != '\0' ||
				    message_buffer_factor < 1 )
				{

					//	i failed.

					//	i print an error message to standard error, log an error
					//	message, set an indicative return status and shut down.

					rep = create_report( INVALID_EVENT_BUFFER_FACTOR ,
					                     optarg ) ;

					fprintf( stderr ,
					         "Error: %s\n" ,
					         rep.string ) ;

					log_error_message( rep.string ) ;

					ret_status = rep.status ;

					goto SHUT_DOWN ;

				}

			break ;

			case 'v' :

				//	logging verbosity option
				//	........................

				//	i found the "-v" command-line option.

				//	i set the the logging verbosity to the value of the option.

				rep = set_logging_verbosity( optarg ) ;

				if( rep.status != SUCCESS )
				{

					//	i failed.

					//	i print an error message to standard error, log an error
					//	message, set an indicative return status and shut down.

					rep = create_report( INVALID_LOGGING_VERBOSITY ,
					                     optarg ) ;

					fprintf( stderr ,
					         "Error: %s\n" ,
					         rep.string ) ;

					log_error_message( rep.string ) ;

					ret_status = rep.status ;

					goto SHUT_DOWN ;

				}

			break ;

			case ':' :

				//	missing value
				//	.............

				//	i found a valid option without an obligatory value.

				//	i print an error message to standard error, log an error
				//	message, set an indicative return status and shut down.

				rep = create_report( MISSING_COMMAND_LINE_VALUE ,
				                     optopt ) ;

				fprintf( stderr ,
				         "Error: %s\n" ,
				         rep.string ) ;

				log_error_message( rep.string ) ;

				ret_status = rep.status ;

				goto SHUT_DOWN ;

			break ;

			case '?' :

				//	invalid option
				//	..............

				//	i found an invalid option.

				//	i print an error message to standard error, log an error
				//	message, set an indicative return status and shut down.

				rep = create_report( INVALID_COMMAND_LINE_OPTION ,
				                     optopt ) ;

				fprintf( stderr ,
				         "Error: %s\n" ,
				         rep.string ) ;

				log_error_message( rep.string ) ;

				ret_status = rep.status ;

				goto SHUT_DOWN ;

			break ;

			default :

				//	catch all else
				//	..............

				//	i found an unimplemented option.

				//	i ignore it and continue.

			break ;

		} 

	}

	//	command-line arguments
	//	----------------------

	//	i discover and check the command-line arguments.

	if( argc - optind < 2 )
   	{

		//	i found less than two command-line arguments.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( INSUFFICIENT_COMMAND_LINE_ARGUMENTS ,
		                     ( argc - optind ) ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;


	}

	//	start up
	//	========

	//	i do as much as i can prior to daemonisation, and shut down immediately
	//	with an error status if i encounter a non-trivial problem.  i have
	//	logging and the standard file descriptors, and i write any significant
	//	messages to both.  i thus create a persistent record in the system log
	//	(which is useful if i am started non-interactively) and provide
	//	immediate notification to standard output and error (which is useful if
	//	i am started interactively or by a script that monitors my output).

	//	log progress
	//	------------

	//	i log that i am starting up.

	log_notice_message( "i am starting up." ) ;

	fprintf( stderr ,
	         "%s\n" ,
	         "i am starting up." ) ;

	log_debug_message( "invocation_name is «%s»."   , invocation_name ) ;
	log_debug_message( "message_buffer_factor is «%li»." , message_buffer_factor ) ;

	//	signals
	//	-------

	//	up until this point, each signal i have received has carried out its
	//	default action, but i now want to register signal handlers to trigger
	//	shut down, and change logging verbosity.  i first build the empty and
	//	full signal sets.  i then block all blockable signals so that each such
	//	signal that arrives while i am registering the handlers will remain
	//	pending.  then, while the signals are blocked, i register the set
	//	shut-down flag function to handle the INT and TERM signals, and
	//	register the increment and decrement logging verbosity functions to
	//	handle respectively the USR1 and USR2 signals.  i then unblock all
	//	blockable signals so that each such signal that arrived while i was
	//	registering the handlers and arrives thereafter will carry out its
	//	registered action if it has one, or its default action otherwise.
	//	finally, i open a signal pipe to which a signal handler can write each
	//	signal it receives, so that if i include the read end of the pipe in an
	//	epoll then i can discover whether the handler has received a signal in
	//	the interval before reading from the epoll (and hence blocking) but
	//	after last checking for signals prior to reading.  i assign the write
	//	end of the pipe to a global variable so that any signal handler can
	//	access it.

	//	build empty signal set
	//	......................

	//	i try to build the empty signal set.

	ret = sigemptyset( &empty_signal_set ) ; 

	log_debug_message( "sigemptyset( &empty_signal_set ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( INIT_SIGNAL_SET_FAILURE ,
		                     &empty_signal_set ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	build full signal set
	//	.....................

	//	i try to build the full signal set.

	ret = sigfillset( &full_signal_set ) ; 

	log_debug_message( "sigfillset( &full_signal_set ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( INIT_SIGNAL_SET_FAILURE ,
		                     &full_signal_set ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	block signals
	//	.............

	//	i try to block all blockable signals.

	ret = sigprocmask( SIG_SETMASK ,
	                   &full_signal_set ,
	                   NULL ) ;

	log_debug_message( "sigprocmask( ... , &full_signal_set , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_MASK_FAILURE ,
		                     &full_signal_set ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	register INT and TERM signal handler
	//	....................................

	//	i build the set shut-down flag action.

	set_shut_down_flag_action.sa_handler = &set_shut_down_flag ;
	set_shut_down_flag_action.sa_mask    = full_signal_set ;
	set_shut_down_flag_action.sa_flags   = 0 ; 

	//	i use the set shut-down flag action to try to register the set
	//	shut-down flag function to handle the INT signal.

	ret = sigaction( SIGINT ,
	                 &set_shut_down_flag_action ,
	                 NULL ) ;

	log_debug_message( "sigaction( SIGINT , &set_shut_down_flag_action , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_ACTION_FAILURE ,
		                     SIGINT ,
		                     &set_shut_down_flag_action ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	i use the set shut-down flag action to try to register the set
	//	shut-down flag function to handle the TERM signal.

	ret = sigaction( SIGTERM ,
	                 &set_shut_down_flag_action ,
	                 NULL ) ;

	log_debug_message( "sigaction( SIGTERM , &set_shut_down_flag_action , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_ACTION_FAILURE ,
		                     SIGTERM ,
		                     &set_shut_down_flag_action ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	register USR1 signal handler
	//	............................

	//	i build the increment logging verbosity action.

	increment_logging_verbosity_action.sa_handler = &increment_logging_verbosity ;
	increment_logging_verbosity_action.sa_mask    = full_signal_set ;
	increment_logging_verbosity_action.sa_flags   = 0 ; 

	//	i use the increment logging verbosity action to try to register the
	//	increment logging verbosity function to handle the USR1 signal.

	ret = sigaction( SIGUSR1 ,
	                 &increment_logging_verbosity_action ,
	                 NULL ) ;

	log_debug_message( "sigaction( SIGUSR1 , &increment_logging_verbosity_action , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_ACTION_FAILURE ,
		                     SIGUSR1 ,
		                     &increment_logging_verbosity_action ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	register USR2 signal handler
	//	............................

	//	i build the decrement logging verbosity action.

	decrement_logging_verbosity_action.sa_handler = &decrement_logging_verbosity ;
	decrement_logging_verbosity_action.sa_mask    = full_signal_set ;
	decrement_logging_verbosity_action.sa_flags   = 0 ; 

	//	i use the decrement logging verbosity action to try to register the
	//	decrement logging verbosity function to handle the USR2 signal.

	ret = sigaction( SIGUSR2 ,
	                 &decrement_logging_verbosity_action ,
	                 NULL ) ;

	log_debug_message( "sigaction( SIGUSR2 , &decrement_logging_verbosity_action , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_ACTION_FAILURE ,
		                     SIGUSR2 ,
		                     &decrement_logging_verbosity_action ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	unblock signals
	//	...............

	//	i try to unblock all blockable signals.

	ret = sigprocmask( SIG_SETMASK ,
	                   &empty_signal_set ,
	                   NULL ) ;

	log_debug_message( "sigprocmask( ... , &empty_signal_set , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( SET_SIGNAL_MASK_FAILURE ,
		                     &empty_signal_set ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	open signal pipe
	//	................

	//	i try to open the signal pipe.

	ret = pipe( signal_pipe ) ;

	log_debug_message( "pipe returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( OPEN_PIPE_FAILURE ) ; 

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	log_debug_message( "signal_pipe is «%i,%i»." , signal_pipe[ 0 ] , signal_pipe[ 1 ] ) ;

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	assign signal pipe ends
	//	.......................

	//	i assign the read and write ends of the signal pipe.

	signal_pipe_read = signal_pipe[ 0 ] ;
	signal_pipe_write = signal_pipe[ 1 ] ;

	log_debug_message( "signal_pipe_read is «%i»." , signal_pipe_read ) ;
	log_debug_message( "signal_pipe_write is «%i»." , signal_pipe_write ) ;

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	source and target directories
	//	-----------------------------

	//	the source directory is the directory i transfer files from, the target
	//	directories are the directories i transfer files to, and the target
	//	count is the number of target directories.  the first command-line
	//	argument specifies the source directory, and the other command-line
	//	arguments specify the target directories.

	//	a name of a file may change at any time.  consequently, a path may
	//	indicate different files at different times.  for example, the path
	//	"/foo" names different files before and after executing the
	//	commands

	//		mv /foo /baz ;
	//		touch /foo ;

	//	unfortunately, many system calls and library functions take a path as
	//	an argument.  for example, the first argument to the stat and access
	//	sytem calls is a path.  consequently, if i call

	//		stat( "/foo" , ... ) ;
	//		access( "/foo" , ... ) ;

	//	then i cannot guarantee that i obtain status and permission information
	//	about the same file, since the path "/foo" may indicate different files
	//	when i call stat and when i call access.

	//	fortunately, a file descriptor indicates the same file no matter what
	//	the names of the file, and many system calls and library functions
	//	take, or have a variant that takes, a file descriptor rather than a
	//	path as an argument.  for example, the fstat system call is identical
	//	to the stat system call except that the first argument to fstat is a
	//	file descriptor.  also, each member of the *at family of system calls
	//	takes a file descriptor that indicates a directory as an argument,
	//	takes a path as an argument, and acts upon the file indicated by
	//	(roughly speaking) the path relative to the file descriptor.  for
	//	example, the faccessat system call operates in exactly the same way as
	//	the access system call, but takes a file descriptor that indicates a
	//	directory as its first argument, takes a path as its second argument,
	//	and delivers permission information about the file indicated by the
	//	path relative to the file descriptor.  thus, if the file descriptor
	//	"foo" indicates a directory, and i call

	//		fstat( foo , ... ) ;
	//		faccessat( foo , "." , ... ) ;

	//	then i can guarantee that i obtain status and permission information
	//	about the same directory.

	//	sadly, these circumventions have two limitations.  firstly, each member
	//	of the *at family of system calls still takes a path as an argument.
	//	if the file descriptor "foo" indicates a directory, and i call

	//		fstat( foo , ... ) ;
	//		faccessat( foo , "." , ... ) ;

	//	then i can guarantee that i obtain status and permission information
	//	about the same directory, because the directory indicated by the path
	//	"." relative to the directory indicated by the file descriptor "foo" is
	//	exactly the directory indicated by the file descriptor "foo".  but if
	//	the file descriptor "foo" indicates the directory "/foo", the file
	//	descriptor "bar" indicates the *non-directory* file "/foo/bar", and i
	//	call

	//		fstat( bar , ... ) ;
	//		faccessat( foo , "bar" , ... ) ;

	//	then i cannot guarantee that i obtain status and permission information
	//	about the same file, because the file indicated by the file descriptor
	//	"bar" when i call fstat may no longer be the file indicated by the path
	//	"bar" relative to the directory indicated by the file descriptor "foo"
	//	when i call faccessat (if someone executes

	//		mv /foo/bar /baz ;
	//		touch /foo/bar ;

	// 	between the fstat and the faccessat calls, for example).  thus, members
	// 	of the *at family of system calls can only be made immune to path
	// 	changes on directories.

	//	secondly, not all system calls and library functions have path-free
	//	alternatives.  for example, the second argument to the
	//	inotify_add_watch system call is a path, and there is no alternative
	//	system call that takes a file descriptor instead.  luckily, there is a
	//	workaround that can usually circumvent this problem too, though it
	//	again only works for a file descriptor that indicates a directory.  the
	//	workaround exploits the facts that the path "." always names the
	//	current working directory, and the current working directory is
	//	unaffected by name changes. thus, if the file descriptor "foo"
	//	indicates a directory and i call

	//		fstat( foo , ... ) ;
	//		faccessat( foo , "." , ... ) ;
	//		fchdir( foo ) ;
	//		inotify_add_watch( ... , "." , ... ) ;

	//	then i can guarantee that i obtain status and permission information
	//	about, and add an inotify watch for file-system events on, the same
	//	directory.  of course, the workaround requires that either
	//
	//		changing my working directory does not affect the rest of my code,
	//		and the rest of my code does not change my current working
	//		directory between my fchdir call and my inotify_add_watch call, or
	//
	//		only one section of code that is sensitive to the current working
	//		directory can execute at any time.

	//	in order to insulate my code from changes to my working directory, i
	//	convert the command line arguments to absolute paths (the source and
	//	target paths), and then open the absolute paths to obtain file
	//	descriptors (the source and target file descriptors).  therefore, the
	//	directory named by a source or target path or indicated by a source or
	//	target file descriptor does not change if my working directory changes.
	//	however, as detailed above, the directory named by a source or target
	//	path may not remain the directory indicated by the corresponding source
	//	or target file descriptor.  i consider the source and target file
	//	descriptors to be authoritative, that is, the source and target
	//	directories *are* the directories indicated by the source and target
	//	file descriptors, while the source and target paths are merely
	//	unreliable descriptions of the source and target directories.  from
	//	this point onwards, i use the source and target file descriptors almost
	//	exclusively, and use the source and target paths only for user
	//	messages.

	//	despite all of the foregoing, there still remains a race condition.
	//	the directory named by a command-line argument may change at any time
	//	between my invocation and the creation of the corresponding file
	//	descriptor.  consequently, the directory indicated by a source or
	//	target file descriptor may not be the directory the invoker intended at
	//	the moment of invocation.  unfortunately, i think that this race
	//	condition is unavoidable, and the invoker must ensure or check that the
	//	directories named by the command-line arguments do not change while i
	//	start up.

	//	in order to create the source and target file descriptors, the source
	//	and target directories must be existing directories, and i must have
	//	read permission on them.  moreover, after starting up, i will transfer
	//	each file written into the source directory to the target directories
	//	by first creating a hard link in each target directory to the file in
	//	the source directory, and then unlinking the file in the source
	//	directory.  in order to create the hard links in the target directories
	//	and unlink the file in the source directory, i must have write and
	//	access permissions on the directories, and they must all be on the same
	//	device.  i test for these requirements.

	//	create source path
	//	..................

	//	i try to expand the first command-line argument to a canonical absolute
	//	path and assign this to the source path.

	//	note: calling realpath with second argument NULL allocates heap memory
	//	to the source path that must be released when no longer needed.

	source_path = realpath( argv[ optind ] ,
	                        NULL ) ;

	log_debug_message( "source path is «%s»." , source_path ) ;

	if( source_path == NULL )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( REALPATH_FAILURE ,
		                     argv[ optind ] ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	open source file descriptor
	//	...........................

	//	i try to open the source file descriptor.

	source_fd = open( source_path ,
	                  O_RDONLY | O_DIRECTORY ) ;

	log_debug_message( "source_fd is «%i»." , source_fd ) ;

	if( source_fd < 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( OPEN_FD_FAILURE ,
		                     source_path ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	test source file descriptor
	//	...........................

	//	i try to test whether i have read, write and access permissions on the
	//	source directory using my effective user and group credentials.

	//	note: strictly, i need not test i have read permission on the source
	//	directory, since i must have had read permission in order to open the
	//	source file descriptor.  nonetheless, i test for read permission again,
	//	since it costs nothing extra, and makes the code more robust to
	//	changes.

	ret = faccessat( source_fd ,
	                 "." ,
	                 R_OK | W_OK | X_OK ,
	                 AT_EACCESS ) ;

	log_debug_message( "faccessat( source_fd , \".\" , R_OK | W_OK | X_OK , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( RWX_PERMISSION_FAILURE ,
		                     source_path ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	find source device id
	//	.....................

	//	i try to stat the source file descriptor.

	ret = fstat( source_fd ,
	             &source_stat ) ;

	log_debug_message( "fstat( source_fd , ... ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( STAT_FAILURE ,
		                     source_path ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	i extract the source device id from the source stat.

	source_device_id = source_stat.st_dev ;

	log_debug_message( "source_device_id is «%ji»." , (intmax_t) source_device_id ) ;

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	set target count
	//	................

	//	i set the target count to the number of target directories, that is,
	//	the number of command-line arguments less 1 (the command-line argument
	//	that specifies the source directory).

	target_count = ( argc - optind ) - 1 ;

	log_debug_message( "target_count is «%i»." , target_count ) ;

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	create target path array
	//	........................

	//	i allocate sufficient heap memory to the target path array to hold one
	//	char pointer for each target directory.

	target_path_array = (char**) malloc( target_count * sizeof( char* ) ) ;

	if( target_path_array == NULL )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ALLOCATE_MEMORY_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	create target file descriptor array
	//	...................................

	//	i allocate sufficient heap memory to the target file descriptor array
	//	to hold one int for each target directory.

	target_fd_array = (int*) malloc( target_count * sizeof( int ) ) ;

	if( target_fd_array == NULL )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ALLOCATE_MEMORY_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	initialise target paths and target file descriptors
	//	...................................................

	//	i initially set each target path to NULL and each target directory to
	//	-1 to indicate that i have not yet allocated heap memory to the path or
	//	opened the file descriptor.  i can subsequently test these values to
	//	determine whether or not i should release heap memory from a path or
	//	close a file descriptor. 

	for( i = 0 ;
	     i < target_count ;
	     ++i )
	{

		target_path_array[ i ] = NULL ;
		target_fd_array[   i ] = -1 ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	for( i = 0 ;
	     i < target_count ;
	     ++i )
	{

		//	create target path
		//	..................

		//	i try to expand the i+1-th command-line argument to a canonical
		//	absolute path, and assign this to the i-th target path.

		//	note: calling realpath with second argument NULL allocates heap
		//	memory to the target path that must be released when no longer
		//	needed.

		target_path_array[ i ] = realpath( argv[ optind + i + 1 ] ,
		                                   NULL ) ;

		log_debug_message( "target_path_array[ %i ] is «%s»." , i , target_path_array[ i ] ) ;

		if( target_path_array[ i ] == NULL )
		{

			//	i failed.

			//	i print an error message to standard error, log an error
			//	message, set an indicative return status and shut down.

			rep = create_report( REALPATH_FAILURE ,
			                     argv[ optind + i + 1 ] ) ;

			fprintf( stderr ,
			         "Error: %s\n" ,
			         rep.string ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	open target file descriptor
		//	...........................

		//	i try to open the target file descriptory.

		target_fd_array[ i ] = open( target_path_array[ i ] ,
		                             O_RDONLY | O_DIRECTORY ) ;

		log_debug_message( "target_fd_array[ %i ] is «%i»." , i , target_fd_array[ i ] ) ;

		if( target_fd_array[ i ] < 0 )
		{

			//	i failed.

			//	i print an error message to standard error, log an error
			//	message, set an indicative return status and shut down.

			rep = create_report( OPEN_FD_FAILURE ,
			                     target_path_array[ i ] ) ;

			fprintf( stderr ,
			         "Error: %s\n" ,
			         rep.string ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	test target file descriptor
		//	...........................

		//	i try to test whether i have read, write and access permissions on
		//	the target directory using my effective user and group credentials.

		//	note: strictly, i need not test i have read permission on the
		//	target directory, since i must have had read permission in order to
		//	open the target file descriptor.  nonetheless, i test for read
		//	permission again, since it costs nothing extra, and makes the code
		//	more robust to changes.

		ret = faccessat( target_fd_array[ i ] ,
		                 "." ,
		                 R_OK | W_OK | X_OK ,
		                 AT_EACCESS ) ;

		log_debug_message( "faccessat( target_fd_array[ %i ] , \".\" , R_OK | W_OK | X_OK , ... ) returned «%i»." ,
		                   i ,  ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i print an error message to standard error, log an error message,
			//	set an indicative return status and shut down.

			rep = create_report( RWX_PERMISSION_FAILURE ,
			                     target_path_array[ i ] ) ;

			fprintf( stderr ,
			         "Error: %s\n" ,
			         rep.string ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	find target device id
		//	.....................

		//	i try to stat the target file descriptor.

		ret = fstat( target_fd_array[ i ] ,
		             &target_stat ) ;

		log_debug_message( "fstat( target_fd_array[ %i ] , ... ) returned «%i»." , i , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i print an error message to standard error, log an error
			//	message, set an indicative return status and shut down.

			rep = create_report( STAT_FAILURE ,
			                     target_path_array[ i ] ) ;

			fprintf( stderr ,
			         "Error: %s\n" ,
			         rep.string ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	i extract the target device id from the target stat.

		target_device_id = target_stat.st_dev ;

		log_debug_message( "target_device_id is «%ji»." , (intmax_t) target_device_id ) ;

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	test target device id
		//	.....................

		//	i compare the source and target device ids.

		if( source_device_id != target_device_id )
		{

			//	they differ.

			//	i print an error message to standard error, log an error
			//	message, set an indicative return status and shut down.

			rep = create_report( DEVICE_FAILURE ,
			                     source_path ,
			                     target_path_array[ i ] ) ;

			fprintf( stderr ,
			         "Error: %s\n" ,
			         rep.string ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

	}

	//	working directory
	//	-----------------

	//	i set my working directory to the source directory, since the source
	//	directory is an existing directory, and i have access permission on it.
	//	moreover, i must later add an inotify watch to the source directory,
	//	and it is easier to ensure that the watch is watching the correct
	//	directory if my working directory is the source directory, since the
	//	path "." always names the working directory (and therefore the source
	//	directory).

	//	set working directory
	//	.....................

	//	i try to change my working directory to the source directory.

	ret = fchdir( source_fd ) ;

	log_debug_message( "fchdir( source_fd ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( FCHDIR_FAILURE ,
		                     source_fd ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	event queue
	//	-----------

	//	i use the inotify subsystem to watch for files written to the source
	//	directory.  i open the event queue, a file descriptor that indicates an
	//	inotify instance, and add the event watch, an inotify watch that writes
	//	an event message to the event queue whenever a file in the source
	//	directory opened for writing is closed.

	//	open event queue
	//	................

	//	i try to open the event queue.

	event_queue = inotify_init1( 0 ) ;

	log_debug_message( "event_queue is «%i»." , event_queue ) ;

	if( event_queue < 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( OPEN_EVENT_QUEUE_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	add event watch
	//	...............

	//	i try to add the event watch to the event queue.

	//	note: the event watch watches the file indicated by the path ".".
	//	since my working directory is the source directory, this ensures that
	//	the event watch watches the source directory, no matter what the names
	//	of the source directory.

	event_watch = inotify_add_watch( event_queue , "." , IN_CLOSE_WRITE ) ;

	log_debug_message( "event_watch is «%i»." , event_watch ) ;

	if( event_watch < 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ADD_EVENT_WATCH_FAILURE , source_path , event_queue ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	epoll
	//	-----

	//	i add the event queue and the read end of the signal pipe to an epoll.
	//	thus, if i read events from the epoll then i read events from both the
	//	event queue and the signal pipe.  i read from the epoll rather than the
	//	event queue in order to be woken from a blocking read by a signal
	//	arriving not during but just prior to the read.  to explain, first
	//	suppose that i read from the event queue.  if i receive a TERM signal
	//	while i am blocked reading from the event queue then the set shut-down
	//	flag function sets the shut-down flag to true, the read returns with an
	//	EINTR error, i test the shut-down flag, find that the flag is true, and
	//	shut down, exactly as i should.  however, if i receive a TERM signal
	//	*after* i last test the shut-down flag but *before* i read from the
	//	event queue then the set shut-down flag function sets the shut-down
	//	flag to true, but the read does not return (at least, not until i
	//	receive another signal or an event is written to the event queue) and i
	//	remain blocked, unable to test the shut-down flag.  but now suppose
	//	that i read from the epoll.  like before, if i receive a TERM signal
	//	while i am blocked reading from the epoll then the set shut-down flag
	//	function sets the shut-down flag to true, the read returns with an
	//	EINTR error, i test the shut-down flag, find that the flag is true, and
	//	shut down.  but, unlike before, if i receive a TERM signal *after* i
	//	last test the shut-down flag but *before* i read from the epoll then
	//	the set shut-down flag function sets the shut-down flag to true and
	//	writes the signal to the signal pipe, the epoll read returns
	//	immediately with the (possibly buffered) signal via the signal pipe, i
	//	test the shut-down flag, find that the flag is true, and shut down.

	//	open epoll
	//	..........

	//	i try to open the epoll.

	epoll = epoll_create1( 0 ) ;

	log_debug_message( "epoll is «%i»." , epoll ) ;

	if( epoll < 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( CREATE_EPOLL_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	add event queue to epoll
	//	........................

	//	i try to add the event queue to the epoll.

	memset( &event_queue_ee ,
	        '\0' ,
	        sizeof( event_queue_ee ) ) ;

	event_queue_ee.events  = EPOLLIN ;
	event_queue_ee.data.fd = EVENT_QUEUE_FD ;

	log_debug_message( "event_queue_ee.events is «%ji»." , (intmax_t) event_queue_ee.events ) ;
	log_debug_message( "event_queue_ee.data.fd is «%i»." , event_queue_ee.data.fd ) ;

	ret = epoll_ctl( epoll ,
	                 EPOLL_CTL_ADD ,
	                 event_queue ,
	                 &event_queue_ee ) ;

	log_debug_message( "epoll_ctl( epoll , EPOLL_CTL_ADD , event_queue , &event_queue_ee ) returned «%i»." , ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ADD_EPOLL_FD_FAILURE ,
		                     epoll ,
		                     event_queue ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	add signal pipe to epoll
	//	........................

	//	i try to add the read end of the signal pipe to the epoll.

	memset( &signal_pipe_ee ,
	        '\0' ,
	        sizeof( signal_pipe_ee ) ) ;

	signal_pipe_ee.events  = EPOLLIN ;
	signal_pipe_ee.data.fd = SIGNAL_PIPE_FD ; 

	log_debug_message( "signal_pipe_ee.events is «%ji»." , (intmax_t) signal_pipe_ee.events ) ;
	log_debug_message( "signal_pipe_ee.data.fd is «%i»." , signal_pipe_ee.data.fd ) ;

	ret = epoll_ctl( epoll ,
	                 EPOLL_CTL_ADD ,
	                 signal_pipe_read ,
	                 &signal_pipe_ee ) ;

	log_debug_message( "epoll_ctl( epoll , EPOLL_CTL_ADD , signal_pipe_read , &signal_pipe_ee ) returned «%i»." ,
	                   ret ) ;

	if( ret != 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ADD_EPOLL_FD_FAILURE ,
		                     epoll ,
		                     signal_pipe_read ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	message buffer
	//	--------------

	//	during the epoll loop, i read messages from the event queue into the
	//	message buffer, ready for separation into individual events for
	//	processing by an event handler.  i assign the message buffer sufficient
	//	memory to contain at least «n» of the largest possible inotify events
	//	that can occur in the source directory, where «n» is the message buffer
	//	factor.

	//	find maximum file name size
	//	...........................

	//	i try to calculate the maximum file name size, the maximum number of
	//	bytes (excluding the terminating null byte) permitted in a filename in
	//	the source directory.

	max_file_name_size = fpathconf( source_fd ,
	                                _PC_NAME_MAX ) ;

	log_debug_message( "max_file_name_size is «%li»." , max_file_name_size ) ;

	if( max_file_name_size == -1 )
	{

		//	i failed or the system does not have a limit.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( NAME_MAX_FAILURE ,
		                     source_path ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	find event sizes
	//	................

	//	i calculate the minimum and maximum event sizes.  the minimum event
	//	size is the number of bytes in an inotify event without the optional
	//	null-terminated file name, and the maximum event size is the number of
	//	bytes in an inotify event with the longest possible optional
	//	null-terminated file name in the source directory.

	min_event_size = sizeof( struct inotify_event ) ;

	max_event_size = min_event_size + max_file_name_size + 1 ;

	log_debug_message( "min_event_size is «%ji»." , (intmax_t) min_event_size ) ;
	log_debug_message( "max_event_size is «%ji»." , (intmax_t) max_event_size ) ;

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	create message buffer
	//	.....................

	//	i allocate sufficient heap memory to the message buffer to hold at
	//	least «n» of the largest possible inotify events that can occur in the
	//	source directory, where «n» is the message buffer factor.

	message_buffer_size = message_buffer_factor * max_event_size ;

	log_debug_message( "message_buffer_size is «%ji»." , (intmax_t) message_buffer_size ) ;

	message_buffer = malloc( message_buffer_size * sizeof( char ) ) ;

	log_debug_message( "message_buffer is «%p»." , message_buffer ) ;

	if( message_buffer == NULL )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( ALLOCATE_MEMORY_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	if the shut-down flag is true then i shut down.

	if( shut_down_flag ) goto SHUT_DOWN ;

	//	fork
	//	----

	//	i fork into a parent process and a child process.  the parent process
	//	immediately shuts down while the child process continues as the daemon.

	//	fork
	//	....

	//	i try to fork.

	parent_flag = fork( ) ;

	log_debug_message( "fork( ) returned «%ji»." , (intmax_t) parent_flag ) ;

	if( parent_flag < 0 )
	{

		//	i failed.

		//	i print an error message to standard error, log an error message,
		//	set an indicative return status and shut down.

		rep = create_report( FORK_FAILURE ) ;

		fprintf( stderr ,
		         "Error: %s\n" ,
		         rep.string ) ;

		log_error_message( rep.string ) ;

		ret_status = rep.status ;

		goto SHUT_DOWN ;

	}

	//	the parent flag is set to true if, and only if, i am the parent
	//	process.

	log_debug_message( "parent_flag is «%ji»." , (intmax_t) parent_flag ) ;

	//	child process
	//	=============

	if( ! parent_flag )
	{

		//	i am the child process.

		//	log progress
		//	------------

		//	i log that i have started up.

		log_notice_message( "i have started up." ) ;

		fprintf( stderr ,
		         "%s\n" ,
		         "i have started up." ) ;

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	standard file descriptors
		//	-------------------------

		//	from this point onwards, i communicate only via log messages and no
		//	longer need standard input, output and error.  i therefore try to
		//	close the standard file descriptors, but continue if i fail.

		//	close standard input
		//	....................

		//	i try to close standard input.

		ret = close( STDIN_FILENO ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    STDIN_FILENO ).string ) ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	close standard output
		//	.....................

		//	i try to close standard output.

		ret = close( STDOUT_FILENO ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    STDOUT_FILENO ).string ) ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	close standard error
		//	....................

		//	i try to close standard error.

		ret = close( STDERR_FILENO ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    STDERR_FILENO ).string ) ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	daemonise
		//	---------

		//	i convert myself into a daemon by first making myself the leader of
		//	a new session and the leader of a new process group within that
		//	session.  this breaks any existing connection to a controlling
		//	terminal, and, since i do not open a controlling terminal myself, i
		//	therefore have no controlling terminal.  i then clear my umask so
		//	that i can have no influence on the permissions of any file or
		//	directory i might create.

		//	create session
		//	..............

		//	i try to create a new session with myself as its leader.

		pid = setsid( ) ;

		log_debug_message( "setsid returned «%ji»." , (intmax_t) pid ) ;

		if( pid < 0 )
		{

			//	a failure occurred.

			//	i log an error message, set an indicative return status and shut
			//	down.

			rep = create_report( SESSION_FAILURE ) ;

			log_error_message( rep.string ) ;

			ret_status = rep.status ;

			goto SHUT_DOWN ;

		}

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	clear umask
		//	...........

		//	i clear my umask.  umask always succeeds and requires no checking.

		umask( 0 ) ;

		//	if the shut-down flag is true then i shut down.

		if( shut_down_flag ) goto SHUT_DOWN ;

		//	loop
		//	====

		//	i now start an infinite loop, during each itaration of which i wait
		//	for messages to arrive on the event queue or the signal pipe that
		//	make up the epoll.

		//	i set the components of the arbitrary epoll event to unused values.

		ee.events  = 0 ;
		ee.data.fd = 0 ;

		while( 1 )
		{

			log_debug_message( "poll loop starting." ) ;

			//	epoll wait
			//	----------

			//	i try to wait for traffic on the epoll.

			ret = epoll_wait( epoll , &ee , 1 , -1 ) ;

			log_debug_message( "epoll_wait returned «%i»." , ret ) ;
			log_debug_message( "ee.events is «%ji»." , (intmax_t) ee.events ) ;
			log_debug_message( "ee.data.fd is «%i»." , ee.data.fd ) ;

			//	epoll wait returns a negative 
			//	.............................

			if( ret < 0 )
			{

				//	an error occurred.

				log_debug_message( "errno is «%i (%s)»." , errno , strerror( errno ) ) ;

				//	i distinguish between the EINTR error and all other errors.

				switch( errno )
				{

					case EINTR :

						//	the epoll received a signal.

						//	if i have a signal handler registered for the
						//	signal then the signal handler has already run.  in
						//	particular, the set shut-down flag signal handler
						//	may have set the shut-down flag to true.

						//	if the shut-down flag is true then i shut down.

						if( shut_down_flag ) goto SHUT_DOWN ;

						//	i restart the read loop.

						continue ;

					break ;

					default :

						//	i failed.

						//	this should never occur!

						//	i log an error message, set an indicative return status and
						//	shut down.

						rep = create_report( EPOLL_WAIT_FAILURE , epoll ) ;

						log_error_message( rep.string ) ;

						ret_status = rep.status ;

						goto SHUT_DOWN ;

					break ;

				}

			}

			//<WORKING>//////////////////////////////////////////////////////////////

			//	epoll wait returns zero
			//	.......................

			if( ret == 0 )
			{

				//	TODO: could it be that this is causing problems when the
				//	event queue is overloaded?  could this be responsible for
				//	the # paths i see when i hammer the source directory?
				//	perhaps io should shut down if this occurs, at least while
				//	developing, to detrmine whether this case ever actually
				//	arises.

				//	the epoll timed out before receiving a signal.  XXX: or i
				//	read 0 events, but returned anyway.

				//  this should never occur!

				//	i log a warning message and restart the poll loop.

				log_warning_message( create_report( EPOLL_TIME_OUT_FAILURE , epoll ).string ) ;

				continue ;

			}

			//	epoll wait returns a positive
			//	.............................

			switch( ee.data.fd )
			{

				////////////////////////////////////////////////////////////////

				//	TODO: i must check if the event queue has overflowed.

				//	got it!  there are flags in the inotify event itself that can
				//	be checked.  these are under the mask property.  something like
				//	event.mask | IN_Q_OVERFLOW should do it.  there are a bunch of
				//	other conditions here that could also be checked - the file
				//	system was unmounted, the watch was removed, etc.  doesn't my
				//	empty event check do something similar.  not sure how these
				//	relate.

				////////////////////////////////////////////////////////////////

				case EVENT_QUEUE_FD :

					//	the epoll received notice that the event queue has traffic.

					log_debug_message( "event queue has traffic." ) ;

					//	read events
					//	...........

					//	i try to read count bytes from the event queue into the
					//	message buffer.

					count = read( event_queue , message_buffer , message_buffer_size ) ;

					log_debug_message( "count is «%zi»." , count ) ;

					if( count < 0 )
					{

						log_debug_message( "errno is «%i %s»." , errno , strerror( errno ) ) ;

						switch( errno )
						{

							////////////////////////////////////////////////////////////////

							//	TODO: are there any other errors i should handle
							//	other than by shutting down?

							////////////////////////////////////////////////////////////////

							case EINTR :

								//	the event queue received a signal.

								//	if the shut-down flag is true then i shut down.

								if( shut_down_flag ) goto SHUT_DOWN ;

								//	i restart the read loop.

								continue ;

							break ;

							default :

								//	i failed.

								//	i log an error message, set an indicative
								//	return status and shut down.

								rep = create_report( READ_EVENT_FAILURE , event_queue ) ;

								log_error_message( rep.string ) ;

								ret_status = rep.status ;

								goto SHUT_DOWN ;

							break ;

						}

					}

					if( count == 0 )
					{

						//	i read a null event from the event queue.

						//	i log an error message, set an indicative return status
						//	and shut down.

						rep = create_report( READ_NULL_EVENT_FAILURE , event_queue ) ;

						log_error_message( rep.string ) ;

						ret_status = rep.status ;

						goto SHUT_DOWN ;

					}

					//	inner loop
					//	..........

					//	i set the offset to the beginning of the read buffer.

					offset = 0 ;

					//	i start the inner loop.

					while( offset < count )
					{

						log_debug_message( "offset is «%zi»." , offset ) ;

						//	the buffer has content at the current offset.

						log_debug_message( "( (struct inotify_event*) &message_buffer[ offset ] )->wd is «%i»." ,
						                    ( (struct inotify_event*) &message_buffer[ offset ] )->wd ) ;
						log_debug_message( "( (struct inotify_event*) &message_buffer[ offset ] )->mask is «%ji»." ,
						         (intmax_t) ( (struct inotify_event*) &message_buffer[ offset ] )->mask ) ;
						log_debug_message( "( (struct inotify_event*) &message_buffer[ offset ] )->cookie is «%ji»." ,
						         (intmax_t) ( (struct inotify_event*) &message_buffer[ offset ] )->cookie ) ;
						log_debug_message( "( (struct inotify_event*) &message_buffer[ offset ] )->len is «%ji»." ,
						         (intmax_t) ( (struct inotify_event*) &message_buffer[ offset ] )->len ) ;
						log_debug_message( "( (struct inotify_event*) &message_buffer[ offset ] )->name is «%s»." ,
						                    ( (struct inotify_event*) &message_buffer[ offset ] )->name ) ;

						////////////////////////////////////////////////////////////////

						//	TODO: i can check for queue overflow here.  instead of
						//	an if test, make this a switch test and check for the
						//	overflow etc.

						////////////////////////////////////////////////////////////////

						if( ( ( (struct inotify_event*) &message_buffer[ offset ] )->mask & IN_IGNORED ) )
						{

							//	the event signals that the event watch ended.

							//	i log an error message, set an indicative return
							//	status and shut down.

							rep = create_report( WATCH_ENDED_FAILURE ,
							                     event_queue ,
							                     ( (struct inotify_event*) &message_buffer[ offset ] )->wd ) ;

							log_error_message( rep.string ) ;

							ret_status = rep.status ;

							goto SHUT_DOWN ;

						}
						else
						{

							//	i pass the event at the current offset to the
							//	close-write-event handler.

							handle_close_write_event( &message_buffer[ offset ] ,
							                          source_path ,
							                          source_fd ,
							                          target_count ,
							                          target_path_array ,
							                          target_fd_array ) ;

							//	i advance the offset to just beyond the end of the message.

							offset += min_event_size + ( (struct inotify_event*) &message_buffer[ offset ] )->len ;

						}

					}

				break ;

				case SIGNAL_PIPE_FD :

					//	the epoll received notice that the signal pipe has traffic.

					log_debug_message( "signal pipe has traffic." ) ;

					////////////////////////////////////////////////////////////////

					//	TODO.  write code to shut down if a signal arrives after
					//	the poll loop starts but before i wait on the epoll. 

					////////////////////////////////////////////////////////////////

				break ;

				default :

					//	the epoll received notice that an unknown file descriptor
					//	has traffic.  this should never occur!

					//	i log an error message, set an indicative return status and
					//	shut down.

					rep = create_report( UNKNOWN_EPOLL_EVENT_FAILURE , epoll , ee.data.fd ) ;

					log_error_message( rep.string ) ;

					ret_status = rep.status ;

					goto SHUT_DOWN ;

				break ;

			}

			//	if the shut-down flag is true then i shut down.

			if( shut_down_flag ) goto SHUT_DOWN ;

		}

	}

	//	shut down
	//	=========

	//	i release resources, continuing through any failures.

	SHUT_DOWN: ;

	//	log progress
	//	------------

	//	if i am the child process then i log that i am shutting down.

	if( ! parent_flag ) log_notice_message( "i am shutting down." ) ;

	//	message buffer
	//	--------------

	//	release message buffer
	//	......................

	if( message_buffer != NULL )
	{

		//	i have allocated heap memory to the message buffer.

		//	i copy the message buffer.

		ptr = message_buffer ;

		//	i set the message buffer to an error value to indicate it is no
		//	longer in service.

		message_buffer = NULL ;

		//	i release the heap memory i allocated to the former message buffer
		//	by releasing the heap memory allocated to the copy.

		free( ptr ) ;

	}

	//	epoll
	//	-----

	//	close epoll
	//	...........

	if( epoll != -1 )
	{

		//	i have opened the epoll.

		//	i copy the epoll.

		fd = epoll ;

		//	i set the epoll to an error value to indcate that it is no longer
		//	in service.

		epoll = -1 ;

		//	i try to close the former epoll by trying to close the copy.

		ret = close( fd ) ;

		log_debug_message( "close( epoll ) returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_EPOLL_FAILURE ,
			                                    epoll ).string ) ;

		}

	}

	//	event queue
	//	-----------

	//	disable event watch
	//	...................

	if( event_watch != -1 )
	{

		//	i have added the event watch to the event queue.

		//	i set the event watch to an error value to indicate that it is no
		//	longer in service.

		event_watch = -1 ;

	}

	//	close event queue
	//	.................

	if( event_queue != -1 )
	{

		//	i have opened the event queue.

		//	i copy the event queue.

		fd = event_queue ;

		//	i set the event queue to an error value to indicate that it is no
		//	longer in service.

		event_queue = -1 ;

		//	i try to close the former event queue by trying to close the copy.

		ret = close( fd ) ;

		log_debug_message( "close( event_queue ) returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_EVENT_QUEUE_FAILURE ,
			                                    event_queue ).string ) ;

		}

	}

	//	source and target directories
	//	-----------------------------

	//	release target file descriptor array
	//	....................................

	if( target_fd_array != NULL )
	{

		//	i have allocated heap memory to the target file-descriptor array.

		for( i = 0 ;
		     i < target_count ;
		     ++i )
		{

			//	close target file descriptor
			//	............................

			if( target_fd_array[ i ] != -1 )
			{

				//	i have opened the target file descriptor.

				//	i copy the target file descriptor.

				fd = target_fd_array[ i ] ;

				//	i set the target file descriptor to an error value to
				//	indicate that it is no longer in service. 

				target_fd_array[ i ] = -1 ;

				//	i try to close the former target file descriptor by trying
				//	to close the copy.

				ret = close( fd ) ;

				log_debug_message( "close( target_fd_array[ %i ] ) returned «%i»." , i , ret ) ;

				if( ret != 0 )
				{

					//	i failed.

					//	i log a warning message and continue.

					log_warning_message( create_report( CLOSE_FD_FAILURE ,
					                                    target_fd_array[ i ] ).string ) ;

				}

			}

		}

		//	i copy the target file descriptor array.

		ptr = target_fd_array ;

		//	i set the target file-descriptor array to an error value to
		//	indicate that it is no longer in service.

		target_fd_array = NULL ;

		//	i release the heap memory i allocated to the former target
		//	file-descriptor array by releasing the heap memory allocated to the
		//	copy.

		free( ptr ) ;

	}

	//	release target path array
	//	.........................

	if( target_path_array != NULL )
	{

		//	i have allocated heap memory to the target path array.

		for( i = 0 ;
		     i < target_count ;
		     ++i )
		{

			//	release target path
			//	...................

			if( target_path_array[ i ] != NULL )
			{

				//	i have allocated heap memory to the target path.

				//	i copy the target path.

				ptr = target_path_array[ i ] ;

				//	i set the target path to an error value to indicate that it
				//	is no longer in service.

				target_path_array[ i ] = NULL ;

				//	i release the heap memory i allocated to the former target
				//	path by releasing the heap memory allocated to the copy.

				free( ptr ) ;

			}

		}

		//	i copy the target path array.

		ptr = target_path_array ;

		//	i set the target path array to an error value to indicate that it
		//	is no longer in service.

		target_path_array = NULL ;

		//	i release the heap memory i allocated to the former target path
		//	array by releasing the heap memory allocated to the copy.

		free( ptr ) ;

	}

	//	close source file descriptor
	//	............................

	if( source_fd != -1 )
	{

		//	i have opened the source file descriptor.

		//	i copy the source file descriptor.

		fd = source_fd ;

		//	i set the source file descriptor to an error value to indicate that
		//	it is no longer in service.

		source_fd = -1 ;

		//	i try to close the former source file descriptor by trying to close
		//	the copy.

		ret = close( fd ) ;

		log_debug_message( "close( source_fd ) returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    source_fd ).string ) ;

		}

	}

	//	release source path
	//	...................

	if( source_path != NULL )
	{

		//	i have allocated heap memory to the source path.

		//	i copy the source path.

		ptr = source_path ;

		//	i set the source path to an error value to indicate that it is no
		//	longer in service.

		source_path = NULL ;

		//	i release the heap memory i allocated to the former source path by
		//	releasing the heap memory allocated to the copy.

		free( ptr ) ;

	}

	//	signals
	//	-------

	//	close signal pipe
	//	.................

	if( signal_pipe_read != -1 )
	{

		//	i have opened the read end of the signal pipe.

		//	i copy the read end of the pipe.

		fd = signal_pipe_read ; 

		//	i set the read end of the pipe to an error value to indicate that
		//	it is no longer in service.

		signal_pipe_read = -1 ;

		//	i try to close the former read end of the signal pipe by trying to
		//	close the copy.

		ret = close( fd ) ;

		log_debug_message( "close( signal_pipe_read ) returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    signal_pipe_read ).string ) ;

		}

	}

	if( signal_pipe_write != -1 )
	{

		//	i have opened the write end of the signal pipe. 

		//	i copy the write end of the pipe.

		fd = signal_pipe_write ;

		//	i set the write end of the pipe to an error value to indicate that
		//	it is no longer in service.

		signal_pipe_write = -1 ;

		//	i try to close the former write end of the signal pipe by trying to
		//	close the copy.

		ret = close( fd ) ;

		log_debug_message( "close( signal_pipe_write ) returned «%i»." , ret ) ;

		if( ret != 0 )
		{

			//	i failed.

			//	i log a warning message and continue.

			log_warning_message( create_report( CLOSE_FD_FAILURE ,
			                                    signal_pipe_write ).string ) ;

		}

	}

	//	log progress
	//	------------

	//	if i am the child process then i log that i have shut down.

	log_debug_message( "shut_down_flag is «%i»." , shut_down_flag ) ;
	log_debug_message( "ret_status is «%i»." , ret_status ) ;

	if( ! parent_flag ) log_notice_message( "i have shut down." ) ;

	//	finalise
	//	========

	//	i finalise logging and return.

	//	logging
	//	-------

	//	i finalise the logging.

	finalise_logging( ) ;

	//	return
	//	------

	//	i return my return status.

	return ret_status ;

}

