#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main ( int    argc ,
           char** argv )
{

	const char* USAGE_STRING = "usage:\n\n\t%s «prefix» «min_suffix» «max_suffix» «cycle_count»\n\ncreates files with the name «prefix».«suffix» in the current directory, where\n\n\t«suffix», «min_suffix» and «max_suffix» are integers, and\n\n\t0 ≤ «min_suffix» ≤ «suffix» ≤ «max_suffix»;\n\nand does so «cycle_count» times, where\n\n\t«cycle_count» is an integer, and\n\n\t1 ≤ «cycle_count».\n\n" ;

	char* invocation_name ;

	char* prefix ;
	int   min_suffix ;
	int   max_suffix ;
	int   cycle_count ;
	char* file_name ;
	int   suffix ;
	int   file_descriptor ;

	int ret ;
	int i ;

	//	get invocation name
	//	-------------------

	invocation_name = argv[ 0 ] ;

	for ( i = 0 ;
	      argv[ 0 ][ i ] != '\0' ;
	      ++i )
	{

		if ( argv[ 0 ][ i ] == '/' )
		{

			invocation_name = argv[ 0 ] + i + 1 ;

		}

	}

	//	check command line
	//	------------------

	if ( argc != 5 )
	{

		fprintf ( stderr ,
		          USAGE_STRING ,
		          invocation_name ) ;

		return 1 ;

	}

	prefix      = argv[ 1 ] ;
	min_suffix  = atoi ( argv[ 2 ] ) ;
	max_suffix  = atoi ( argv[ 3 ] ) ;
	cycle_count = atoi ( argv[ 4 ] ) ;

	if ( min_suffix  < 0          ||
	     max_suffix  < min_suffix ||
	     cycle_count < 1 )
	{

		fprintf ( stderr ,
		          USAGE_STRING ,
		          invocation_name ) ;

		return 1 ;

	}

	//	prepare file name
	//	-----------------

	file_name = calloc ( strlen( argv[ 1 ] ) + strlen( argv[ 3 ] ) + 2 ,
	                     sizeof ( char ) ) ;

	if ( file_name == NULL )
	{

		perror( "could not allocate memory" ) ;

	}

	//	touch files
	//	-----------

	for ( i = 0 ;
	      i < cycle_count ;
	      ++i )
	{

		for ( suffix  = min_suffix ;
		      suffix <= max_suffix ;
		      ++suffix )
		{

			//	construct file name
			//	...................

			sprintf( file_name ,
			         "%s.%i" ,
			         prefix ,
			         suffix ) ;

			//	open file
			//	.........

			file_descriptor = open ( file_name ,
			                         O_WRONLY | O_CREAT ,
			                         S_IRUSR | S_IWUSR ) ;

			if ( file_descriptor == -1 )
			{

				perror( "could not open file" ) ;

			}

			//	close file
			//	..........

			ret = close( file_descriptor ) ;

			if ( ret == -1 )
			{

				perror( "could not close file" ) ;

			}

		}

	}

	return 0 ;

}

